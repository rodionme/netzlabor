global.config = {
  build: {
    src: {
      html: [
        './src/views/**/*.pug',
        '!./src/views/**/_*.pug'
      ],
      css: './src/styles/main.scss',
      js: './src/js/**/*.js',
      js_vendor: './src/vendor/**/*.js',
      img: 'src/img/**/*.{png,jpg,gif,svg}',
    },
    dest: {
      css: 'dist/css',
      html: 'dist/',
      js: 'dist/js',
      img: 'dist/img',
      full: 'dist/**/*.*'
    }
  },
  watch: {
    css: 'src/{styles,vendor}/**/*.scss',
    html: 'src/views/**/*.pug',
    js: 'src/js/**/*.js',
    js_vendor: 'src/vendor/**/*.js',
    img: 'src/img/**/*.{png,jpg,gif,svg}',
  },
  browsers: ['last 2 versions', 'not ie < 11']
};

exports.config = config;