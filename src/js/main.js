'use strict';

// Polyfill for Element.closest()
if (!Element.prototype.matches) {Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;}if (!Element.prototype.closest) {Element.prototype.closest = function (s) {var el = this;if (!document.documentElement.contains(el)) {return null;}do {if (el.matches(s)) {return el;}el = el.parentElement || el.parentNode;} while (el !== null && el.nodeType === 1);return null;};}

document.getElementById('menu-trigger').addEventListener('click', function (e) {
  var trigger = document.getElementById('menu-trigger');
  var sidebar = document.getElementById('sidebar');

  if (trigger.classList.contains('active')) {
    trigger.classList.remove('active');
    sidebar.classList.remove('active');
  } else {
    trigger.classList.add('active');
    sidebar.classList.add('active');
  }
});

document.body.addEventListener('click', function (e) {
  var menuItem = e.target.closest('.main-menu__item');

  if (menuItem && menuItem.querySelector('.main-menu__list')) {
    menuItem.classList.toggle('main-menu__item--expanded');
  }
});
